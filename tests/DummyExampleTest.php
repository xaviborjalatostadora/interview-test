<?php

namespace App;


use PHPUnit\Framework\TestCase;

class DummyExampleTest extends TestCase
{
    public function test_it_works()
    {
        $dummy = new DummyExample();
        $this->assertTrue($dummy->dummy());
    }
}
